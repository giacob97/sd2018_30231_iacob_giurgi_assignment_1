package ui;

import business.BillBLL;
import business.ComplaintBLL;
import business.UserBLL;
import dao.model.Bill;
import dao.model.Complaint;
import dao.model.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.List;

public class UserPage extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Button addButton = new Button("Send complaint");
    private Button logOut = new Button("Log out");
    private Label descLabel = new Label("Description");
    private TextArea description = new TextArea();
    private Label serviceLabel = new Label("Service Grade");
    private TextField serviceField = new TextField("");
    private Label customerLabel = new Label("Customer Service Grade");
    private TextField customerField = new TextField("");
    private Label responseLabel = new Label("Response Grade");
    private TextField responseField = new TextField("");
    private UserBLL ub = new UserBLL();
    //private User u = ub.findUserById(2);
    public User u = new User();
    private BillBLL bl = new BillBLL();
    private TableView<Bill> table = new TableView<Bill>();
    private TableView<Bill> table1 = new TableView<Bill>();

    private Label idBill = new Label("Enter the number of bill : ");
    private TextField billField = new TextField();
    private Label sumLabel = new Label("Enter a sum : ");
    private  TextField sumField = new TextField();
    private Button button = new Button("Submit payment");
    Scene scene = new Scene(new Group());
    final Label label = new Label("Client options");


    @Override
    public void start(Stage primaryStage) {


        List<Bill> unpaidList = bl.findBillsByClient(u.getClientno(),"neplatit");
        List<Bill> paidList = bl.findBillsByClient(u.getClientno(),"platit");
        ObservableList<Bill> data = FXCollections.observableArrayList(unpaidList);
        ObservableList<Bill> dataPaid = FXCollections.observableArrayList(paidList);
        //Stage stage = new Stage();
        primaryStage.setTitle("Logged as " + u.getUsername());
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);


        label.setFont(new Font("Arial", 20));
        table.setEditable(true);
        TableColumn firstNameCol = new TableColumn("Price");
        firstNameCol.setMinWidth(100);
        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Bill, Integer>("price"));

        TableColumn lastNameCol = new TableColumn("Status");
        lastNameCol.setMinWidth(100);
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Bill, String>("Status"));

        TableColumn emailCol = new TableColumn("Bill Id");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(
                new PropertyValueFactory<Bill, Integer>("idbill"));

        TableColumn f = new TableColumn("Price");
        f.setMinWidth(100);
        f.setCellValueFactory(
                new PropertyValueFactory<Bill, Integer>("price"));

        TableColumn l= new TableColumn("Status");
        l.setMinWidth(100);
        l.setCellValueFactory(
                new PropertyValueFactory<Bill, String>("Status"));

        TableColumn e = new TableColumn("Bill Id");
        e.setMinWidth(100);
        e.setCellValueFactory(
                new PropertyValueFactory<Bill, Integer>("idbill"));

        table.setItems(data);
        table.getColumns().addAll(firstNameCol, lastNameCol, emailCol);
        table.relocate(250,80);

        table1.setItems(dataPaid);
        table1.getColumns().addAll(f, l, e);
        table1.setEditable(true);
        table1.relocate(250,80);
        table1.setVisible(false);
        table.setVisible(false);

        idBill.relocate(215,185);
        button.relocate(350,350);
        button.setPrefHeight(30);
        billField.setPrefHeight(30);
        billField.relocate(350,180);
        sumLabel.relocate(215,255);
        sumField.setPrefHeight(30);
        sumField.relocate(350,250);
        ((Group) scene.getRoot()).getChildren().addAll(idBill,billField,sumField,sumLabel,button);
        this.payment(false);

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.relocate(330,150);
        gridPane.setHgap(20);
        gridPane.setVgap(20);


        this.complaint(gridPane);
        gridPane.setVisible(false);
        Button viewUnpaid = new Button("Unpaid bills");
        viewUnpaid.setOnAction(ee -> {
            gridPane.setVisible(false);
            payment(false);
            table1.setVisible(false);
            table.setVisible(true);

        });


        Button viewPaid = new Button("Paid bills");
        viewPaid.setOnAction(ee-> {
            gridPane.setVisible(false);
            payment(false);
            table.setVisible(false);
            table1.setVisible(true);

        });


        Button payNow = new Button("Pay now");
        payNow.setOnAction(ee->{
            gridPane.setVisible(false);
            payment(true);
            table.setVisible(false);
            table1.setVisible(false);
            button.setOnAction(ef-> {
                int b = Integer.parseInt(billField.getText());
                int price = Integer.parseInt(sumField.getText());
                bl.payABill(b,u.getClientno(),price);
            });

        });

        Button request = new Button("Request for a feedback");
        request.setOnAction(ee -> {
            gridPane.setVisible(false);
            table.setVisible(false);
            table1.setVisible(false);
            payment(false);
            if(u.getRequestFeedback().equals("no")){
                showAlert(Alert.AlertType.CONFIRMATION, label.getScene().getWindow(), "Sent!", "The request for a complaint has been sended");
                u.setRequestFeedback("yes");
                ub.updateUser(u);
            }
            else {
                if (u.getRequestFeedback().equals("yes")) {
                    showAlert(Alert.AlertType.ERROR, label.getScene().getWindow(), "Sent!", "A request has already been sent");
                }
            }
            if(u.getRequestFeedback().equals("accepted")){
                showAlert(Alert.AlertType.CONFIRMATION, label.getScene().getWindow(), "Sent!", "Check the 'Complete a complaint' section");
            }
        });

        Button complete = new Button("Complete a feedback");
        complete.setOnAction(ee -> {

            table.setVisible(false);
            table1.setVisible(false);
            payment(false);

            if(!u.getRequestFeedback().equals("accepted")){
                showAlert(Alert.AlertType.INFORMATION, label.getScene().getWindow(), "!", "Wait for a confirmation");
            }
            if(u.getRequestFeedback().equals("accepted")){
                //showAlert(Alert.AlertType.INFORMATION, label.getScene().getWindow(), "Complete", "Please complete");
                gridPane.setVisible(true);
                addButton.setOnAction(ef -> {
                    int service = Integer.valueOf(serviceField.getText());
                    int response = Integer.valueOf(responseField.getText());
                    int cs = Integer.valueOf(customerField.getText());
                    Complaint c = new Complaint(u.getUsername(),description.getText(),service,response,cs);
                    ComplaintBLL cb = new ComplaintBLL();
                    cb.insertComplaint(c);
                    u.setRequestFeedback("no");
                    ub.updateUser(u);
                    gridPane.setVisible(false);
                });
            }
        });
        final VBox vbox = new VBox();
        vbox.setSpacing(50);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(50, 0, 0, 10));
        vbox.getChildren().addAll(label,viewPaid,viewUnpaid,payNow,request,complete,logOut);
        ((Group) scene.getRoot()).getChildren().addAll(vbox,table1,table,gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();
        if(this.u.getRequestFeedback().equals("accepted")){
            showAlert(Alert.AlertType.CONFIRMATION, label.getScene().getWindow(), "Message!", "You have received a complaint letter!");
        }

        logOut.setOnAction(ee->{
            Start s = new Start();
            s.setU(new User());
            s.start(primaryStage);
        });

    }


    public void payment(boolean visibility){
        if(visibility == true){
            this.button.setVisible(true);
            this.idBill.setVisible(true);
            this.billField.setVisible(true);
            this.sumField.setVisible(true);
            this.sumLabel.setVisible(true);

        }
        else{
            this.button.setVisible(false);
            this.idBill.setVisible(false);
            this.billField.setVisible(false);
            this.sumField.setVisible(false);
            this.sumLabel.setVisible(false);
        }

    }



    private void complaint(GridPane gridPane){
        description.setPrefSize(40,20);
        Label headerLabel = new Label("Registration");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0,20,0));
        gridPane.add(descLabel,0,1);
        gridPane.add(description,1,1);
        gridPane.add(serviceLabel,0,2);
        gridPane.add(serviceField,1,2);
        gridPane.add(responseLabel,0,3);
        gridPane.add(responseField,1,3);
        gridPane.add(customerLabel,0,4);
        gridPane.add(customerField,1,4);
        gridPane.add(addButton,0,5,2,1);

    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }
}

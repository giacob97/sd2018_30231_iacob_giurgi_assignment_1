package ui;

import business.ComplaintBLL;
import business.UserBLL;
import business.Validators;
import dao.model.Complaint;
import dao.model.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.Date;
import java.util.List;

public class ManagerPage extends Application {


    public User u;
    private UserBLL ub = new UserBLL();
    //public User u = ub.findUserById(1);
    private TableView<User> table = new TableView<>();
    private Button approve = new Button("Approve");
    private Scene scene = new Scene(new Group());
    private final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
    private GridPane gridPane = new GridPane();
    private Button delete = new Button("Delete");
    private Button create = new Button("Save");
    private Button update = new Button("Update");
    private Button clientsButton = new Button("View Clients");
    private Button complaintsButton = new Button("View Complaints");
    private Button requestButton = new Button("View Reports");
    private TextField nameField = new TextField();
    private TextField addresstf = new TextField();
    private TextField cardtf = new TextField();
    private TextField emailtf = new TextField();
    public Button submitButton = new Button("Update");
    private TableView<Complaint> tc = new TableView<>();
    private TextField usernameField = new TextField();
    private TextField clientField = new TextField();
    private TextField typeField = new TextField();
    private TextField responsetf = new TextField();
    private GridPane gridPaneC = new GridPane();
    Button cc = new Button("Save");
    Button cu = new Button("Update");
    Button cd = new Button("Delete");
    private TextField descriptiontf = new TextField();
    private TextField datetf = new TextField();
    private TextField servicetf = new TextField();
    private TextField customeServicetf = new TextField();
    private TextField username1 = new TextField();
    private TextField type1 = new TextField();
    private Button logOut = new Button("Log out");

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Stage stage = new Stage();
        stage.setTitle("Logged as " + u.getUsername());
        stage.setWidth(1400);
        stage.setHeight(700);
        this.viewClients(table);
        this.viewComplaints(tc);
        clientsButton.relocate(80,30);
        clientsButton.setPrefSize(150,30);

        complaintsButton.relocate(400,30);
        complaintsButton.setPrefSize(150,30);

        requestButton.relocate(720,30);
        requestButton.setPrefSize(150,30);


        logOut.relocate(1040,30);
        logOut.setPrefSize(150,30);

        clientsButton.setOnAction(e -> {
            tc.setVisible(false);
            gridPaneC.setVisible(false);
            gridPane.setVisible(true);
            table.setVisible(true);

        });

        complaintsButton.setOnAction(e->{
            table.setVisible(false);
            gridPane.setVisible(false);
            tc.setVisible(true);
            gridPaneC.setVisible(true);

        });



        requestButton.setOnAction(e->{
            ComplaintBLL cb = new ComplaintBLL();
            List<Float> reports = cb.getReports();
            showAlert(Alert.AlertType.CONFIRMATION, stage.getScene().getWindow(), "Message!", "Average grade for service quality is : " + reports.get(0));
            showAlert(Alert.AlertType.CONFIRMATION, stage.getScene().getWindow(), "Message!", "Average grade for response quality is : " + reports.get(1));
            showAlert(Alert.AlertType.CONFIRMATION, stage.getScene().getWindow(), "Message!", "Average grade for cutomer service quality is : " + reports.get(2));
        });

        logOut.setOnAction(ee->{

            Start s = new Start();
            s.setU(new User());
            s.start(stage);
        });

        ((Group) scene.getRoot()).getChildren().addAll(clientsButton,complaintsButton,requestButton,logOut);
        stage.setScene(scene);
        stage.show();



    }

    private void viewClients(TableView<User> table){

        table.setEditable(true);

        TableColumn username = new TableColumn("Username");
        username.setMinWidth(100);
        username.setCellValueFactory(new PropertyValueFactory<User, Integer>("username"));

        TableColumn name = new TableColumn("Name");
        name.setMinWidth(100);
        name.setCellValueFactory(new PropertyValueFactory<User, Integer>("name"));


        TableColumn addresss = new TableColumn("Address");
        addresss.setMinWidth(100);
        addresss.setCellValueFactory(new PropertyValueFactory<User, String>("address"));

        TableColumn cardNumber = new TableColumn("Card number");
        cardNumber.setMinWidth(100);
        cardNumber.setCellValueFactory(new PropertyValueFactory<User, Integer>("cardno"));

        TableColumn cl = new TableColumn("Client number");
        cl.setMinWidth(100);
        cl.setCellValueFactory(new PropertyValueFactory<User, Integer>("clientno"));

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(175);
        emailCol.setCellValueFactory(new PropertyValueFactory<User, String>("email"));

        TableColumn type = new TableColumn("Type");
        type.setMinWidth(175);
        type.setCellValueFactory(new PropertyValueFactory<User, String>("type"));

        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
        table.setItems(data);
        table.getColumns().addAll(username,name,addresss, cardNumber,cl,emailCol,type);
        table.relocate(350,80);

        addUIControls(gridPane);
        table.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                User u = table.getSelectionModel().getSelectedItem();
                usernameField.setText(u.getUsername());
                nameField.setText(u.getName());
                addresstf.setText(u.getAddress());
                cardtf.setText(String.valueOf(u.getCardno()));
                clientField.setText(String.valueOf(u.getClientno()));
                typeField.setText(u.getType());
                emailtf.setText(u.getEmail());
                update.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(nameField.getText().isEmpty() || !Validators.stringWithoutNumbers(nameField.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your username");
                            return;
                        }
                        if(emailtf.getText().isEmpty() || !Validators.isValidEmailId(emailtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your email correct");
                            return;
                        }



                        if(addresstf.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your address");
                            return;
                        }

                        if(cardtf.getText().isEmpty() || !Validators.digitsValidator(cardtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your card number correct");
                            return;
                        }

                        if(clientField.getText().isEmpty() || !Validators.digitsValidator(clientField.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your client number correct");
                            return;
                        }
                        UserBLL bl = new UserBLL();
                        u.setUsername(usernameField.getText());
                        u.setName(nameField.getText());
                        u.setAddress(addresstf.getText());
                        u.setCardno(Integer.parseInt(cardtf.getText()));
                        u.setClientno(Integer.parseInt(clientField.getText()));
                        u.setEmail(emailtf.getText());
                        u.setType(typeField.getText());

                        bl.updateUser(u);
                        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
                        table.setItems(data);
                    }
                });

                create.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(nameField.getText().isEmpty() || !Validators.stringWithoutNumbers(nameField.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your username");
                            return;
                        }
                        if(emailtf.getText().isEmpty() || !Validators.isValidEmailId(emailtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your email correct");
                            return;
                        }



                        if(addresstf.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your address");
                            return;
                        }

                        if(cardtf.getText().isEmpty() || !Validators.digitsValidator(cardtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your card number correct");
                            return;
                        }

                        if(clientField.getText().isEmpty() || !Validators.digitsValidator(clientField.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your client number correct");
                            return;
                        }
                        UserBLL bl = new UserBLL();
                        u.setUsername(usernameField.getText());
                        u.setName(nameField.getText());
                        u.setAddress(addresstf.getText());
                        u.setCardno(Integer.parseInt(cardtf.getText()));
                        u.setClientno(Integer.parseInt(clientField.getText()));
                        u.setEmail(emailtf.getText());
                        u.setType(typeField.getText());
                        u.setPassword("");
                        bl.insertUser(u);
                        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
                        table.setItems(data);
                    }
                });
                delete.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        UserBLL bl = new UserBLL();
                        bl.deleteUser(u.getId());
                        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
                        table.setItems(data);
                    }
                });
            }


        });
        ((Group) scene.getRoot()).getChildren().addAll(table,gridPane);
        table.setVisible(true);
    }


    private void addUIControls(GridPane gridPane) {
        gridPane.relocate(10,10);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        // Add Header
        Label headerLabel = new Label("Infos");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));


        Label usernameLabel = new Label("Username : ");
        gridPane.add(usernameLabel, 0, 1);


        usernameField.setPrefHeight(30);
        gridPane.add(usernameField, 1, 1);

        // Add Name Label
        Label nameLabel = new Label("Name : ");
        gridPane.add(nameLabel, 0, 2);


        nameField.setPrefHeight(30);
        gridPane.add(nameField, 1, 2);

        Label address = new Label("Address : ");
        gridPane.add(address, 0, 3);


        addresstf.setPrefHeight(30);
        gridPane.add(addresstf, 1, 3);


        Label card = new Label("Card number: ");
        gridPane.add(card, 0, 4);


        cardtf.setPrefHeight(30);
        gridPane.add(cardtf, 1, 4);

        Label c = new Label("Client number: ");
        gridPane.add(c, 0, 5);


        clientField.setPrefHeight(30);
        gridPane.add(clientField, 1, 5);


        Label cl = new Label("Email: ");
        gridPane.add(cl, 0, 6);

        // Add Email Text Field

        emailtf.setPrefHeight(30);
        gridPane.add(emailtf, 1, 6);

        Label tp = new Label("Type of user: ");
        gridPane.add(tp, 0, 7);

        // Add Email Text Field

        typeField.setPrefHeight(30);
        gridPane.add(typeField, 1, 7);


        create.setPrefHeight(20);
        create.setDefaultButton(true);
        create.setPrefWidth(100);
        update.setPrefHeight(20);
        update.setDefaultButton(true);
        update.setPrefWidth(100);
        delete.setPrefHeight(20);
        delete.setDefaultButton(true);
        delete.setPrefWidth(100);
        gridPane.add(create, 0, 9,5,1);
        gridPane.add(update, 0, 10,5,1);
        gridPane.add(delete, 0, 11,5,3);
        GridPane.setHalignment(create, HPos.CENTER);
        GridPane.setMargin(create, new Insets(20, 0, 20, 0));

        GridPane.setHalignment(update, HPos.CENTER);
        GridPane.setMargin(update, new Insets(20, 0, 20, 0));
        GridPane.setHalignment(delete, HPos.CENTER);
        GridPane.setMargin(delete, new Insets(20, 0, 20, 0));

    }

    private void viewComplaints(TableView<Complaint> table){

        TableColumn name = new TableColumn("Username");
        name.setMinWidth(100);
        name.setCellValueFactory(new PropertyValueFactory<Complaint, String>("username"));


        TableColumn addresss = new TableColumn("Description");
        addresss.setMinWidth(200);
        addresss.setCellValueFactory(new PropertyValueFactory<Complaint, String>("description"));

        TableColumn cardNumber = new TableColumn("Date");
        cardNumber.setMinWidth(150);
        cardNumber.setCellValueFactory(new PropertyValueFactory<Complaint, Date>("date"));

        TableColumn sg = new TableColumn("Service quality");
        sg.setMinWidth(50);
        sg.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("serviceGrade"));

        TableColumn response = new TableColumn("Response quality");
        response.setMinWidth(50);
        response.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("responseGrade"));

        TableColumn csq = new TableColumn("Customer service quality");
        csq.setMinWidth(50);
        csq.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("customeServiceGrade"));

        this.addUIControls2(gridPaneC);

        final ObservableList<Complaint> data = FXCollections.observableArrayList(new ComplaintBLL().findAll());
        table.setItems(data);
        table.setPrefHeight(400);
        table.setPrefWidth(800);
        table.getColumns().addAll(name,addresss, cardNumber,sg,response,csq);
        table.relocate(350,80);
        table.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                Complaint c = table.getSelectionModel().getSelectedItem();
                username1.setText(c.getUsername());
                descriptiontf.setText(c.getDescription());
                datetf.setText(c.getDate().toString());
                servicetf.setText(String.valueOf(c.getServiceGrade()));
                responsetf.setText(String.valueOf(c.getResponseGrade()));
                customeServicetf.setText(String.valueOf(c.getCustomeServiceGrade()));
                cu.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(descriptiontf.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a description");
                            return;
                        }
                        if(customeServicetf.getText().isEmpty() || !Validators.digitsValidator(customeServicetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for customer service");
                            return;
                        }

                        if(servicetf.getText().isEmpty() || !Validators.digitsValidator(servicetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for service");
                            return;
                        }
                        if(responsetf.getText().isEmpty() || !Validators.digitsValidator(responsetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for response");
                            return;
                        }

                        ComplaintBLL bl = new ComplaintBLL();
                        c.setUsername(username1.getText());
                        c.setDescription(descriptiontf.getText());
                        c.setServiceGrade(Integer.parseInt(servicetf.getText()));
                        c.setResponseGrade(Integer.parseInt(responsetf.getText()));
                        c.setCustomeServiceGrade(Integer.parseInt(customeServicetf.getText()));

                        bl.updateComplaint(c);
                        final ObservableList<Complaint> data = FXCollections.observableArrayList(bl.findAll());
                        table.setItems(data);
                    }
                });

                cc.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if(username1.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a username");
                            return;
                        }

                        if(descriptiontf.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a description");
                            return;
                        }
                        if(customeServicetf.getText().isEmpty() || !Validators.digitsValidator(customeServicetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for customer service");
                            return;
                        }

                        if(servicetf.getText().isEmpty() || !Validators.digitsValidator(servicetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for service");
                            return;
                        }
                        if(responsetf.getText().isEmpty() || !Validators.digitsValidator(responsetf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPaneC.getScene().getWindow(), "Form Error!", "Please enter a digit for response");
                            return;
                        }
                        ComplaintBLL bl = new ComplaintBLL();
                        c.setUsername(username1.getText());
                        c.setDescription(descriptiontf.getText());
                        c.setServiceGrade(Integer.parseInt(servicetf.getText()));
                        c.setResponseGrade(Integer.parseInt(responsetf.getText()));
                        c.setCustomeServiceGrade(Integer.parseInt(customeServicetf.getText()));

                        bl.insertComplaint(c);
                        final ObservableList<Complaint> data = FXCollections.observableArrayList(bl.findAll());
                        table.setItems(data);
                    }
                });
                cd.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        ComplaintBLL cl = new ComplaintBLL();
                        cl.deleteComplaint(c.getIdComplaint());
                        final ObservableList<Complaint> data = FXCollections.observableArrayList(cl.findAll());
                        table.setItems(data);
                    }
                });
            }


        });

        ((Group) scene.getRoot()).getChildren().addAll(table,gridPaneC);
        table.setVisible(false);
        gridPaneC.setVisible(false);

    }


    private void addUIControls2(GridPane gridPane) {
        
        gridPane.relocate(10,10);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        Label headerLabel = new Label("Infos");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));


        Label usernameLabel = new Label("Username : ");
        gridPane.add(usernameLabel, 0, 1);


        username1.setPrefHeight(30);
        gridPane.add(username1, 1, 1);

        // Add Name Label
        Label nameLabel = new Label("Description : ");
        gridPane.add(nameLabel, 0, 2);


        descriptiontf.setPrefHeight(30);
        gridPane.add(descriptiontf, 1, 2);

        Label address = new Label("Date: ");
        gridPane.add(address, 0, 3);


        datetf.setPrefHeight(30);
        gridPane.add(datetf, 1, 3);


        Label card = new Label("Service grade: ");
        gridPane.add(card, 0, 4);


        servicetf.setPrefHeight(30);
        gridPane.add(servicetf, 1, 4);

        Label c = new Label("Response grade: ");
        gridPane.add(c, 0, 5);


        responsetf.setPrefHeight(30);
        gridPane.add(responsetf, 1, 5);


        Label cl = new Label("Customer service grade: ");
        gridPane.add(cl, 0, 6);



        customeServicetf.setPrefHeight(30);
        gridPane.add(customeServicetf, 1, 6);



        cc.setPrefHeight(20);
        cc.setDefaultButton(true);
        cc.setPrefWidth(100);
        cu.setPrefHeight(20);
        cu.setDefaultButton(true);
        cu.setPrefWidth(100);
        cd.setPrefHeight(20);
        cd.setDefaultButton(true);
        cd.setPrefWidth(100);
        gridPane.add(cc, 0, 9,5,1);
        gridPane.add(cu, 0, 10,5,1);
        gridPane.add(cd, 0, 11,5,3);
        GridPane.setHalignment(cc, HPos.CENTER);
        GridPane.setMargin(cc, new Insets(20, 0, 20, 0));

        GridPane.setHalignment(cu, HPos.CENTER);
        GridPane.setMargin(cu, new Insets(20, 0, 20, 0));
        GridPane.setHalignment(cd, HPos.CENTER);
        GridPane.setMargin(cd, new Insets(20, 0, 20, 0));
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    private boolean checkFields(){
        return true;
    }
    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }
}

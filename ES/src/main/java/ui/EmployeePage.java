package ui;

import business.ComplaintBLL;
import business.UserBLL;
import business.Validators;
import dao.model.Complaint;
import dao.model.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.Date;

public class EmployeePage extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private UserBLL ub = new UserBLL();
    public User u;


    private Scene scene = new Scene(new Group());
    private Button clientsButton = new Button("View Clients");
    private Button complaintsButton = new Button("View Complaints");
    private Button requestButton = new Button("View Request");
    private TextField nameField = new TextField();
    private TextField addresstf = new TextField();
    private TextField cardtf = new TextField();
    private TextField emailtf = new TextField();
    public Button submitButton = new Button("Update");
    private TableView<Complaint> tc = new TableView<>();
    private TableView<User> table = new TableView<>();
    public GridPane gridPane = new GridPane();
    private Button logOut = new Button("Log out");


    @Override
    public void start(Stage primaryStage) {

        Stage stage = new Stage();
        stage.setTitle("Logged as " + u.getUsername());
        stage.setWidth(1200);
        stage.setHeight(600);
        this.viewComplaints(tc);
        this.viewClients(table);

        clientsButton.relocate(80,30);
        clientsButton.setPrefSize(150,30);

        complaintsButton.relocate(400,30);
        complaintsButton.setPrefSize(150,30);

        requestButton.relocate(720,30);
        requestButton.setPrefSize(150,30);

        logOut.relocate(1040,30);
        logOut.setPrefSize(150,30);

        clientsButton.setOnAction(e -> {
            tc.setVisible(false);

            gridPane.setVisible(true);
            table.setVisible(true);

        });

        complaintsButton.setOnAction(e->{
            table.setVisible(false);
            gridPane.setVisible(false);
            tc.setVisible(true);

        });
        Permission p = new Permission();
        requestButton.setOnAction(e->{
            p.start(primaryStage);
        });

        logOut.setOnAction(ee->{

            Start s = new Start();
            s.setU(new User());
            s.start(stage);
        });

        ((Group) scene.getRoot()).getChildren().addAll(clientsButton,complaintsButton,requestButton,logOut);
        stage.setScene(scene);
        stage.show();

    }




    private void viewClients(TableView<User> table){
        final Label label = new Label("Employee options");
        label.setFont(new Font("Arial", 20));
        table.setEditable(true);

        TableColumn name = new TableColumn("Name");
        name.setMinWidth(100);
        name.setCellValueFactory(new PropertyValueFactory<User, Integer>("name"));


        TableColumn addresss = new TableColumn("Address");
        addresss.setMinWidth(100);
        addresss.setCellValueFactory(new PropertyValueFactory<User, String>("address"));

        TableColumn cardNumber = new TableColumn("Card number");
        cardNumber.setMinWidth(100);
        cardNumber.setCellValueFactory(new PropertyValueFactory<User, Integer>("cardno"));

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(175);
        emailCol.setCellValueFactory(new PropertyValueFactory<User, String>("email"));

        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
        table.setItems(data);
        table.getColumns().addAll(name,addresss, cardNumber,emailCol);
        table.relocate(350,80);

        addUIControls(gridPane);
        table.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                User u = table.getSelectionModel().getSelectedItem();
                nameField.setText(u.getName());
                addresstf.setText(u.getAddress());
                cardtf.setText(String.valueOf(u.getCardno()));
                emailtf.setText(u.getEmail());
                submitButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {

                        if(emailtf.getText().isEmpty() || !Validators.isValidEmailId(emailtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your email correct");
                            return;
                        }


                        if(nameField.getText().isEmpty() || !Validators.stringWithoutNumbers(nameField.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your name correct");
                            return;
                        }

                        if(addresstf.getText().isEmpty()) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your address correct");
                            return;
                        }

                        if(cardtf.getText().isEmpty() || !Validators.digitsValidator(cardtf.getText())) {
                            showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter your card number correct");
                            return;
                        }
                        UserBLL bl = new UserBLL();
                        u.setName(nameField.getText());
                        u.setAddress(addresstf.getText());
                        u.setCardno(Integer.parseInt(cardtf.getText()));
                        u.setEmail(emailtf.getText());
                        bl.updateUser(u);
                        showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(), "Update!","Client updated");
                        final ObservableList<User> data = FXCollections.observableArrayList(ub.findAll());
                        table.setItems(data);
                    }
                });
            }


        });
        ((Group) scene.getRoot()).getChildren().addAll(table,gridPane);
        table.setVisible(true);
    }


    private void addUIControls(GridPane gridPane) {
        gridPane.relocate(10,10);
        gridPane.setHgap(10);
        gridPane.setVgap(20);
        // Add Header
        Label headerLabel = new Label("Infos");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));

        // Add Name Label
        Label nameLabel = new Label("Name : ");
        gridPane.add(nameLabel, 0, 1);


        nameField.setPrefHeight(30);
        gridPane.add(nameField, 1, 1);

        Label address = new Label("Address : ");
        gridPane.add(address, 0, 2);


        addresstf.setPrefHeight(30);
        gridPane.add(addresstf, 1, 2);


        Label card = new Label("Card number: ");
        gridPane.add(card, 0, 3);


        cardtf.setPrefHeight(30);
        gridPane.add(cardtf, 1, 3);


        Label cl = new Label("Email: ");
        gridPane.add(cl, 0, 4);

        // Add Email Text Field

        emailtf.setPrefHeight(30);
        gridPane.add(emailtf, 1, 4);

        // Add Submit Button

        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 5, 5, 2);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0, 20, 0));


    }

    private void viewComplaints(TableView<Complaint> table){

        TableColumn name = new TableColumn("Username");
        name.setMinWidth(100);
        name.setCellValueFactory(new PropertyValueFactory<Complaint, String>("username"));


        TableColumn addresss = new TableColumn("Description");
        addresss.setMinWidth(200);
        addresss.setCellValueFactory(new PropertyValueFactory<Complaint, String>("description"));

        TableColumn cardNumber = new TableColumn("Date");
        cardNumber.setMinWidth(150);
        cardNumber.setCellValueFactory(new PropertyValueFactory<Complaint, Date>("date"));

        TableColumn sg = new TableColumn("Service quality");
        sg.setMinWidth(50);
        sg.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("serviceGrade"));

        TableColumn response = new TableColumn("Response quality");
        response.setMinWidth(50);
        response.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("responseGrade"));

        TableColumn csq = new TableColumn("Customer service quality");
        csq.setMinWidth(50);
        csq.setCellValueFactory(new PropertyValueFactory<Complaint,Integer>("customeServiceGrade"));

        final ObservableList<Complaint> data = FXCollections.observableArrayList(new ComplaintBLL().findAll());
        table.setItems(data);
        table.setPrefHeight(400);
        table.setPrefWidth(800);
        table.getColumns().addAll(name,addresss, cardNumber,sg,response,csq);
        table.relocate(100,80);
        ((Group) scene.getRoot()).getChildren().addAll(table);
        table.setVisible(false);

    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }


    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }

}

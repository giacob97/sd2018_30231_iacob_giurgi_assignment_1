package ui;

import business.UserBLL;
import dao.model.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class Permission extends Application {

    private UserBLL ub = new UserBLL();
    private TableView<User> tr = new TableView<>();
    private Button approve = new Button("Approve");
    private Scene scene = new Scene(new Group());
    private final ObservableList<User> data = FXCollections.observableArrayList(ub.findByRequested());

    public static void main(String[] args) {
        launch(args);
    }

    public Permission() {
        this.viewRequest(tr);
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Permission Form");
        primaryStage.setWidth(250);
        primaryStage.setHeight(400);
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    private void viewRequest(TableView<User> table){
        TableColumn name = new TableColumn("Name");
        name.setMaxWidth(100);
        name.setCellValueFactory(new PropertyValueFactory<User, Integer>("name"));
        table.setItems(data);
        table.getColumns().addAll(name);
        table.setPrefWidth(100);
        approve.relocate(120,150);
        approve.setPrefHeight(50);
        approve.setPrefWidth(95);
        ((Group) scene.getRoot()).getChildren().addAll(table,approve);
        approve.setOnAction(e -> {
            for(User u : data){
                u.setRequestFeedback("accepted");
                ub.updateUser(u);
            }
            //Platform.exit();
        });


    }
}

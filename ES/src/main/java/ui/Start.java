package ui;

import business.UserBLL;
import dao.model.User;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;

public class Start extends Application  {
    Stage window;
    UserBLL bl = new UserBLL();
    Button button,signupButton;
    public User u;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
            primaryStage.setTitle("Start Page");
            window = primaryStage;
            GridPane gridPane = loginForm();
            addUIControls(gridPane);
            // Create a scene with registration form grid pane as the root node
            Scene scene = new Scene(gridPane, 400, 400);
             // Set the scene in primary stage
            window.setScene(scene);
            window.show();



    }

    private GridPane loginForm() {
        // Instantiate a new Grid Pane
        GridPane gridPane = new GridPane();

        // Position the pane at the center of the screen, both vertically and horizontally
        gridPane.setAlignment(Pos.CENTER);

        // Set a padding of 20px on each side
        gridPane.setPadding(new Insets(30, 30, 30, 30));

        // Set the horizontal gap between columns
        gridPane.setHgap(10);

        // Set the vertical gap between rows
        gridPane.setVgap(10);

        // Add Column Constraints

        // columnOneConstraints will be applied to all the nodes placed in column one.
        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);

        // columnTwoConstraints will be applied to all the nodes placed in column two.
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200,200, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);

        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);
        return gridPane;
    }

    private void addUIControls(GridPane gridPane) {
        // Add Header
        Label headerLabel = new Label("Welcome!");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0, 20, 0));

        // Add Name Label
        Label nameLabel = new Label("Username : ");
        gridPane.add(nameLabel, 0, 1);

        // Add Name Text Field
        TextField nameField = new TextField();
        nameField.setPrefHeight(30);
        gridPane.add(nameField, 1, 1);

        // Add Password Label
        Label passwordLabel = new Label("Password : ");
        gridPane.add(passwordLabel, 0, 2);

        PasswordField passwordField = new PasswordField();
        passwordField.setPrefHeight(30);
        gridPane.add(passwordField, 1, 2);

        button = new Button();
        button.setText("Sign up");
        button.setOnAction(e -> {
            RegistrationForm rf = new RegistrationForm();
            rf.start(window);
        });
        GridPane.setHalignment(button, HPos.CENTER);
        GridPane.setMargin(button, new Insets(20, 0, 20, 0));
        button.setPrefHeight(40);
        button.setDefaultButton(true);
        button.setPrefWidth(100);



        Button b1 = new Button("Login");
        b1.setText("Login");
        b1.setOnAction(e -> {
            u = bl.findByUsername(nameField.getText());
            if(u==null){
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Error!", "Please enter a valid username");
            }
            else{
                if(!u.getPassword().equals(passwordField.getText())){
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Error!", "Wrong password");
                }
                else{
                    //showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(), "Welcome", "Login successful");
                    this.typeOfUser();
                }
            }

        });
        b1.setPrefHeight(40);
        b1.setDefaultButton(true);
        b1.setPrefWidth(100);
        gridPane.add(b1, 0, 5, 2, 1);
        gridPane.add(button, 0, 6, 2, 1);
        GridPane.setHalignment(b1, HPos.CENTER);
        GridPane.setMargin(b1, new Insets(20, 0, 20, 0));

    }
    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    private void typeOfUser(){

            if(u.getType().equals("client")){
                UserPage userPage = new UserPage();
                userPage.setU(u);
                userPage.start(window);
            }
            else{
                if(u.getType().equals("employee")){
                    EmployeePage employeePage = new EmployeePage();
                    employeePage.setU(u);
                    employeePage.start(window);
                }
                else{
                    if(u.getType().equals("manager")){
                        ManagerPage managerPage = new ManagerPage();
                        managerPage.setU(u);
                        managerPage.start(window);
                    }
                    else{
                        showAlert(Alert.AlertType.ERROR, window.getScene().getWindow(), "Error!", "Unavailable user type");
                    }
                }
            }
    }


    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }
}

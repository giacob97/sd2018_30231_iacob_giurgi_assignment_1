package business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

    public static boolean stringWithoutNumbers(String string){
        String regexUserName = "^[A-Za-z\\s]+$";
        Pattern pattern = Pattern.compile(regexUserName,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(string);
        return matcher.find();

    }

    public static boolean digitsValidator(String string){

        Pattern pattern = Pattern.compile("[0-9]+"); //correct pattern for both float and integer.
        pattern = Pattern.compile("\\d+");
        return pattern.matcher(string).matches();
    }

    public static boolean isValidEmailId(String email) {
        String emailPattern = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern p = Pattern.compile(emailPattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

}

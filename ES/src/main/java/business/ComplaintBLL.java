package business;

import dao.model.Complaint;
import dao.operations.ComplaintRepo;

import java.util.List;

public class ComplaintBLL {
    private ComplaintRepo complaintRepo;

    public ComplaintBLL(){
        this.complaintRepo = new ComplaintRepo();
    }

    public Complaint findComplaintById(int id) {
        Complaint complaintById = complaintRepo.findById(id);
        if (complaintById == null) {
            System.out.println("The user with id= " + id + " was not found.");
        }

        return complaintById;
    }


    public List<Complaint> findAll() {
        List<Complaint> userAll = (List<Complaint>) complaintRepo.findAll();
        return userAll;
    }


    public Complaint insertComplaint(Complaint insertedComplaint) {

        return complaintRepo.save(insertedComplaint);
    }


    public Complaint updateComplaint(Complaint updatedComplaint) {

        return complaintRepo.update(updatedComplaint);
    }


    public Complaint deleteComplaint(int deletedComplaint) {

        return complaintRepo.delete(deletedComplaint);
    }

    public List<Float> getReports(){
        return complaintRepo.getReports();
    }
}

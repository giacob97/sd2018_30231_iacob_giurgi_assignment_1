package business;

import dao.model.Bill;
import dao.operations.BillRepo;
import java.util.List;

public class BillBLL {

    private BillRepo billRepo;

    public BillBLL(){
        this.billRepo = new BillRepo();
    }

    public Bill findBillById(int id) {
        Bill billById = billRepo.findById(id);
        if (billById == null) {
            System.out.println("The bill with id= " + id + " was not found.");
        }

        return billById;
    }


    public List<Bill> findAll() {
        List<Bill> userAll = (List<Bill>) billRepo.findAll();
        return userAll;
    }


    public Bill insertBill(Bill insertedBill) {

        return billRepo.save(insertedBill);
    }


    public Bill updateBill(Bill updatedBill) {

        return billRepo.update(updatedBill);
    }


    public Bill deleteBill(int deletedBill) {

        return billRepo.delete(deletedBill);
    }


    public void payABill(int id,int clientNo,int price){
        billRepo.payABill(id,clientNo,price);
    }

    public List<Bill> findBillsByClient(int id , String paid){
        return billRepo.findUnpaidByClientNo(id,paid);
    }

}

package business;

import dao.model.User;
import dao.operations.UserRepo;
import java.util.List;

public class UserBLL {

    private UserRepo userRepo;

    public UserBLL(){
        this.userRepo = new UserRepo();
    }

    public User findUserById(int id) {
        User userById = userRepo.findById(id);
        if (userById == null) {
           System.out.println("The user with id= " + id + " was not found.");
        }

        return userById;
    }


    public List<User> findAll() {
        List<User> userAll = (List<User>) userRepo.findAll();
        return userAll;
    }


    public User insertUser(User insertedUser) {

        return userRepo.save(insertedUser);
    }


    public User updateUser(User updatedUser) {

        return userRepo.update(updatedUser);
    }


    public User deleteUser(int deletedUser) {

        return userRepo.delete(deletedUser);
    }

    public User findByUsername(String username){
        return userRepo.findByUsername(username);
    }
    public List<User> findByRequested() {return userRepo.findByRequested();}

}

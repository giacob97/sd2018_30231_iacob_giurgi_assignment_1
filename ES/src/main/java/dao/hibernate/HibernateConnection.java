package dao.hibernate;

import org.hibernate.cfg.Configuration;
import org.hibernate.*;
public class HibernateConnection {

    public HibernateConnection() {

    }

    public SessionFactory initialize() {
        SessionFactory sessionFactory=new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        return sessionFactory;
    }


}

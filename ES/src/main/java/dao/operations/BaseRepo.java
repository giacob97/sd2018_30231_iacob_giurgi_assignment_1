package dao.operations;

import dao.hibernate.HibernateConnection;
import dao.interfaces.BaseRepoInterface;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public abstract class BaseRepo<T> implements BaseRepoInterface<T>{

      protected SessionFactory sessionFactory;
      private HibernateConnection hibernateConnection;
      private Class<T> clazz;


    public BaseRepo(Class<T> clazz) {
        this.clazz=clazz;
        hibernateConnection=new HibernateConnection();
        this.sessionFactory=hibernateConnection.initialize();
    }

    @SuppressWarnings({ "rawtypes", "unused" })
    public T findById(int id) {
        Session session = sessionFactory.openSession();
        T entity = session.get(clazz, id);
        session.close();
        return entity;
    }


    @SuppressWarnings({ "rawtypes", "unused" })
    public T save(T entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
        } catch (RuntimeException ex) {
            if (tx != null)
                tx.rollback();
        } finally {
            session.close();
        }
        return null;
    }

    @SuppressWarnings({ "rawtypes", "unused" })
    public 	T delete(int id){
        Session session = sessionFactory.openSession();
        try{
            session.beginTransaction();
            Query query = session.createQuery("delete from " + clazz.getSimpleName() + " where id"+clazz.getSimpleName()+"=" + id);
            query.executeUpdate();
            session.beginTransaction().commit();
            return null;
        } catch(RuntimeException ex){
            System.out.println(ex.getMessage());
        }finally{
            session.close();
        }
        return null;
    }
    @SuppressWarnings({ "rawtypes", "unused" })
    public T update(T tToUpdate){
        Session session = sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx = session.beginTransaction();
            session.update(tToUpdate);
            tx.commit();
            session.close();
            return tToUpdate;

        } catch(RuntimeException ex){
            if (tx!=null)
                tx.rollback();
            System.out.println(ex.getMessage());
        }finally{
            session.close();
        }
        return null;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" ,"unused"})

    public List<T> findAll() {
        List<T> lists = new ArrayList<T>();
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List result = session.createQuery("from "+clazz.getSimpleName()).list();
            for (T event : (List<T>) result) {
                lists.add(event);
            }
            tx.commit();

        } catch (RuntimeException ex) {
            if (tx != null)
                tx.rollback();
            System.out.println(ex.getMessage() + " Unable to find all");
        } finally {
            session.close();
        }

        return lists;
    }

}

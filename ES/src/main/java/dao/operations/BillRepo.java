package dao.operations;

import dao.model.Bill;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class BillRepo extends BaseRepo<Bill>{

     public BillRepo(){
         super(Bill.class);
     }


     public void payABill(int id,int clientNo,int price){
         Session session = super.sessionFactory.openSession();
         String unpaid = "neplatit";
         List<Bill> result = session.createQuery("from" + " Bill" + " where clientno=" + clientNo+ " and status='" + unpaid + "'" + " and idbill=" + id).list();
         //System.out.print(result.size());
         Bill b = result.get(0);
         int oldRest = b.getPrice();
         int newRest = b.getPrice() - price;
         if(newRest == 0){
             b.setStatus("platit");
         }
         else{
             b.setPrice(newRest);
         }
         super.update(b);
     }



     public List<Bill> findUnpaidByClientNo(int id,String unpaid){
         List<Bill> listOfUnpaid = new ArrayList<Bill>();
         char [] u = unpaid.toCharArray();
         Session session = super.sessionFactory.openSession();
         List<Bill> aux = super.findAll();
         Transaction tx = null;
         try {
             tx = session.beginTransaction();
             List result = session.createQuery("from" + " Bill" + " where clientno=" + id+ " and status='" + unpaid + "'").list();
             for (Bill event : (List<Bill>) result) {
                 listOfUnpaid.add(event);
             }

             tx.commit();

         } catch (RuntimeException ex) {
             if (tx != null)
                 tx.rollback();
             System.out.println(ex.getMessage() + " Unable to find all");
         } finally {
             session.close();
         }
         return listOfUnpaid;
     }


}

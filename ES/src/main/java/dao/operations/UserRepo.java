package dao.operations;

import dao.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class UserRepo extends BaseRepo<User> {

    public UserRepo(){
        super(User.class);
    }

    public User findByUsername(String username){
        List<User> users = super.findAll();
        for(User u : users){
            if(u.getUsername().equals(username)){
                return u;
            }
        }
        return null;
    }

    public List<User> findByRequested(){
        List<User> lists = new ArrayList<User>();
        String yes= "yes";
        Session session = super.sessionFactory.openSession();
        Transaction tx = null;
        tx = session.beginTransaction();
        List<User> result = session.createQuery("from " + "User " + "where requestFeedback=" + "'" + yes +"'" ).list();
        for (User event : (List<User>) result) {
            lists.add(event);
        }
        tx.commit();
        return lists;
    }

}

package dao.operations;

import dao.model.Complaint;

import java.util.ArrayList;
import java.util.List;

public class ComplaintRepo extends BaseRepo<Complaint> {

    public ComplaintRepo() {
        super(Complaint.class);
    }


    public List<Float> getReports(){
        List<Complaint> list = super.findAll();
        List<Float> result = new ArrayList<Float>();
        int size = list.size();
        int service = 0;
        int response = 0;
        int customer = 0;
        for(Complaint c : list){
            service =service + c.getServiceGrade();
            response = response + c.getResponseGrade();
            customer = customer + c.getCustomeServiceGrade();
        }
        float a = (float)service / size ;
        float b = (float)response / size;
        float c = (float) customer / size;
        result.add(a);
        result.add(b);
        result.add(c);
        return result;
    }
}

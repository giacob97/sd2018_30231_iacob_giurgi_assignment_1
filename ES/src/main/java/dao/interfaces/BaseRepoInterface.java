package dao.interfaces;

import java.util.List;

public interface BaseRepoInterface<T> {


    public T findById(int id);
    public T save(T entity);
    public T delete(int id);
    public T update(T tToUpdate);
    public List<T> findAll();


}

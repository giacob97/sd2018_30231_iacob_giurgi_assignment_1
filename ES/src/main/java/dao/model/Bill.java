package dao.model;

public class Bill {

    private int idbill;
    private int price;
    private String status;
    private int clientno;


    public Bill(int price, String status,int a) {
        this.price = price;
        this.status = status;
        this.clientno = a;
    }
    public Bill(){

    }

    public int getIdbill() {
        return idbill;
    }

    public void setIdbill(int idbill) {
        this.idbill = idbill;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int getClientno() {
        return clientno;
    }

    public void setClientno(int iu) {
        this.clientno = iu;
    }
}

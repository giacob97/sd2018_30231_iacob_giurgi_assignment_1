package dao.model;


//@Entity
//@Table(name="user")
public class User {

    public User(){

    }

    //@Id
    //@Column(name = "iduser")
    //@GeneratedValue(generator = "incrementator")
    //@GenericGenerator(name = "incrementator" , strategy = "increment")
    private int id;

    //@Column(name = "username")
    private String username;

    //@Column (name = "password")
    private String password;

    private String email;


    //@Column (name ="name")
    private String name;

    //@Column (name = "address")
    private String address;

    //@Column (name = "type")
    private String type = "client";

    //@Column (name = "cardno")
    private int cardno;

    //@Column (name = "clientno")
    private int clientno;


    private String requestFeedback;

    public User(String username, String password, String name,String email, String address, int cardno, int clientno) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.address = address;
        this.cardno = cardno;
        this.clientno = clientno;
        this.requestFeedback = "no";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCardno() {
        return cardno;
    }

    public void setCardno(int cardno) {
        this.cardno = cardno;
    }

    public int getClientno() {
        return clientno;
    }

    public void setClientno(int clientno) {
        this.clientno = clientno;
    }

    public String getRequestFeedback() {
        return requestFeedback;
    }

    public void setRequestFeedback(String requestFeedback) {
        this.requestFeedback = requestFeedback;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

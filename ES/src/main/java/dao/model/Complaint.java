package dao.model;

import java.util.Calendar;
import java.util.Date;

public class Complaint {

    private int idComplaint;
    private String username;
    private String description;
    private int serviceGrade;
    private int responseGrade;
    private int customeServiceGrade;
    private Date date;


    public Complaint(String username, String description, int serviceGrade, int responseGrade, int customeServiceGrade) {
        this.username = username;
        this.description = description;
        this.serviceGrade = serviceGrade;
        this.responseGrade = responseGrade;
        this.customeServiceGrade = customeServiceGrade;
        this.getToday();
    }

    public Complaint(){

    }

    public void getToday(){
        Calendar myCal = Calendar.getInstance();
        myCal.set(2018,3,17);
        date = myCal.getTime();
    }
    public int getIdComplaint() {
        return idComplaint;
    }

    public void setIdComplaint(int idComplaint) {
        this.idComplaint = idComplaint;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getServiceGrade() {
        return serviceGrade;
    }

    public void setServiceGrade(int serviceGrade) {
        this.serviceGrade = serviceGrade;
    }

    public int getResponseGrade() {
        return responseGrade;
    }

    public void setResponseGrade(int responseGrade) {
        this.responseGrade = responseGrade;
    }

    public int getCustomeServiceGrade() {
        return customeServiceGrade;
    }

    public void setCustomeServiceGrade(int customeServiceGrade) {
        this.customeServiceGrade = customeServiceGrade;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
